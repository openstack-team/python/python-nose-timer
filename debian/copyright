Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: nose-timer
Source: https://github.com/mahmoudimus/nose-timer

Files: *
Copyright: (c) 2012, Juan Pedro Fisanotti <fisadev@gmail.com>
           (c) 2014, Mahmoud Abdelkader <mahmoud@balancedpayments.com>
           (c) 2012, Andres Riancho <andres.riancho@gmail.com>
           (c) 2013-2014, Ivan Kolodyazhny <ikolodyazhny@mirantis.com>
           (c) Kevin Burke
           (c) 2014, Anton Egorov <anton.egoroff@gmail.com>
           (c) 2013, Dmitry Sandalov <d.sandalov@netrika.ru>
           (c) 2014, Stanislav Kudriashev <skudriashev@griddynamics.com>
License: BSD-and-Expat-dual-license

Files: debian/*
Copyright: (c) 2014-2019, Thomas Goirand <zigo@debian.org>
License: BSD-and-Expat-dual-license

License: BSD-and-Expat-dual-license
 MIT License
 .
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 BSD License
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
  1. Redistributions of source code must retain the above copyright
     notice, this list of conditions and the following disclaimer.
  2. Redistributions in binary form must reproduce the above copyright
     notice, this list of conditions and the following disclaimer in the
     documentation and/or other materials provided with the distribution.
  3. All advertising materials mentioning features or use of this software must
     display the following acknowledgement: This product includes software
     developed by Balanced Inc.
  4. Neither the name of Balanced Inc. nor the names of its contributors may be
     used to endorse or promote products derived from this software without
     specific prior written permission.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
